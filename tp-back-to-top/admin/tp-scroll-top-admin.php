<?php

	if(empty($_POST['tp_scroll_to_top_hidden']))
		{			
		$tp_scroll_top_option_enable = get_option( 'tp_scroll_top_option_enable' );
		$tp_scroll_top_visibility_trigger = get_option( 'tp_scroll_top_visibility_trigger' );
		$tp_scroll_top_visibility_fade_speed = get_option( 'tp_scroll_top_visibility_fade_speed' );
		$tp_scroll_top_scroll_fade_speed = get_option( 'tp_scroll_top_scroll_fade_speed' );
		$tp_scroll_top_scroll_position = get_option( 'tp_scroll_top_scroll_position' );
		$tp_scroll_top_scrollbg = get_option( 'tp_scroll_top_scrollbg' );
		$tp_scroll_top_scrollbg_hover = get_option( 'tp_scroll_top_scrollbg_hover' );
		$tp_scroll_top_scrollradious = get_option( 'tp_scroll_top_scrollradious' );
			
		}

	else
		{
		
		if($_POST['tp_scroll_to_top_hidden'] == 'Y')
		{
		//Form data sent

		$tp_scroll_top_visibility_trigger = sanitize_text_field( $_POST['tp_scroll_top_visibility_trigger'] );
		update_option('tp_scroll_top_visibility_trigger', $tp_scroll_top_visibility_trigger);
		$tp_scroll_top_visibility_fade_speed = sanitize_text_field( $_POST['tp_scroll_top_visibility_fade_speed'] );
		update_option('tp_scroll_top_visibility_fade_speed', $tp_scroll_top_visibility_fade_speed);
		$tp_scroll_top_scroll_fade_speed = sanitize_text_field( $_POST['tp_scroll_top_scroll_fade_speed'] );
		update_option('tp_scroll_top_scroll_fade_speed', $tp_scroll_top_scroll_fade_speed);	
		$tp_scroll_top_scroll_position = sanitize_text_field( $_POST['tp_scroll_top_scroll_position']);
		update_option('tp_scroll_top_scroll_position', $tp_scroll_top_scroll_position);
		$tp_scroll_top_option_enable = sanitize_text_field( $_POST['tp_scroll_top_option_enable'] );
		update_option('tp_scroll_top_option_enable', $tp_scroll_top_option_enable);
		$tp_scroll_top_scrollbg = sanitize_text_field( $_POST['tp_scroll_top_scrollbg'] );
		update_option('tp_scroll_top_scrollbg', $tp_scroll_top_scrollbg);
		$tp_scroll_top_scrollbg_hover = sanitize_text_field( $_POST['tp_scroll_top_scrollbg_hover'] );
		update_option('tp_scroll_top_scrollbg_hover', $tp_scroll_top_scrollbg_hover);
		$tp_scroll_top_scrollradious = sanitize_text_field( $_POST['tp_scroll_top_scrollradious'] );
		update_option('tp_scroll_top_scrollradious', $tp_scroll_top_scrollradious);
		
		?>
		<div class="updated"><p><strong><?php _e('Changes Saved.' ); ?></strong></p>
		</div>


		<?php
		}
		} 
		?>


	<div class="wrap">
	<?php echo "<h2>".__('Tp Scroll Top Settings')."</h2>";?>

	<form  method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<input type="hidden" name="tp_scroll_to_top_hidden" value="Y">
        <?php settings_fields( 'tp_scroll_to_top_plugin_options' );
			  do_settings_sections( 'tp_scroll_to_top_plugin_options' );
		?>
        <table class="form-table">
			<tr valign="top">
				<th scope="row"><label for="tp_scroll_top_option_enable">Enabled:</label></th>
				<td style="vertical-align:middle;">
				<select name="tp_scroll_top_option_enable">
					<option value="true" <?php if($tp_scroll_top_option_enable=='true') echo "selected"; ?> >Show</option>
					<option value="false" <?php if($tp_scroll_top_option_enable=='false') echo "selected"; ?> >Hide</option>                              
				</select><br>
				<span style="font-size:12px;color:#000000">Use Dropdown Menu to Select Scroll To Top enable/disable.</span>
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><label for="tp_scroll_top_visibility_trigger">Visibility Trigger:</label></th>
				<td style="vertical-align:middle;">
				<select name="tp_scroll_top_visibility_trigger">
					<option value="0" <?php if($tp_scroll_top_visibility_trigger=='0') echo "selected"; ?> >0</option>
					<option value="100" <?php if($tp_scroll_top_visibility_trigger=='100') echo "selected"; ?> >100</option> 
					<option value="400" <?php if($tp_scroll_top_visibility_trigger=='400') echo "selected"; ?> >400</option>                                
				</select><br>
				<span style="font-size:12px;color:#000000">Use Dropdown Menu to Select Scroll To Top enable/disable.</span>
				</td>
			</tr> 
			<tr valign="top">
				<th scope="row"><label for="tp_scroll_top_visibility_fade_speed">Visibility Fade Speed:</label></th>
				<td style="vertical-align:middle;">
				<select name="tp_scroll_top_visibility_fade_speed">
					<option value="0" <?php if($tp_scroll_top_visibility_fade_speed=='0') echo "selected"; ?> >0</option>
					<option value="100" <?php if($tp_scroll_top_visibility_fade_speed=='100') echo "selected"; ?> >100</option> 
					<option value="400" <?php if($tp_scroll_top_visibility_fade_speed=='400') echo "selected"; ?> >400</option> 
					<option value="600" <?php if($tp_scroll_top_visibility_fade_speed=='600') echo "selected"; ?> >600</option> 
					<option value="800" <?php if($tp_scroll_top_visibility_fade_speed=='800') echo "selected"; ?> >800</option>                                
				</select><br>
				<span style="font-size:12px;color:#000000">Use Dropdown Menu to Select Scroll To Top enable/disable.</span>
				</td>
			</tr>  
			<tr valign="top">
				<th scope="row"><label for="tp_scroll_top_scroll_fade_speed">Scroll Speed:</label></th>
				<td style="vertical-align:middle;">
				<select name="tp_scroll_top_scroll_fade_speed">
					<option value="0" <?php if($tp_scroll_top_scroll_fade_speed=='0') echo "selected"; ?> >0</option>
					<option value="100" <?php if($tp_scroll_top_scroll_fade_speed=='100') echo "selected"; ?> >100</option>
					<option value="400" <?php if($tp_scroll_top_scroll_fade_speed=='400') echo "selected"; ?> >400</option>
					<option value="500" <?php if($tp_scroll_top_scroll_fade_speed=='500') echo "selected"; ?> >500</option>
					<option value="600" <?php if($tp_scroll_top_scroll_fade_speed=='600') echo "selected"; ?> >600</option>
					<option value="700" <?php if($tp_scroll_top_scroll_fade_speed=='700') echo "selected"; ?> >700</option>
				</select><br>
				<span style="font-size:12px;color:#000000">Use Dropdown Menu to Select Scroll To Top enable/disable.</span>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="tp_scroll_top_scrollbg">Bg Color</label></th>
				<td style="vertical-align:middle;">
					<input  size='10' name='tp_scroll_top_scrollbg' class='scroll_title_bg' type='text' id="scroll-bg" value='<?php echo esc_attr($tp_scroll_top_scrollbg); ?>' /><br />
					<span style="font-size:12px;color:#000000">select back to top bg color.</span>
				</td>
			</tr>	
			<tr valign="top">
				<th scope="row"><label for="tp_scroll_top_scrollbg_hover">Hover Bg Color</label></th>
				<td style="vertical-align:middle;">
					<input  size='10' name='tp_scroll_top_scrollbg_hover' class='scroll_hover_bg' type='text' id="scroll-hoverbg" value='<?php echo esc_attr($tp_scroll_top_scrollbg_hover); ?>' /><br />
					<span style="font-size:12px;color:#000000">select back to top hover bg color.</span>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row"><label for="tp_scroll_top_scrollradious">Border Radious</label></th>
				<td style="vertical-align:middle;">
					<input type="text" size='10' name='tp_scroll_top_scrollradious' class='scroll_hover_bg' type='text' id="scroll-radious" value='<?php echo esc_attr($tp_scroll_top_scrollradious); ?>' />%<br />
					<span style="font-size:12px;color:#000000">select back to top border radious.default border radious 50%</span>
				</td>
			</tr>			
			
			<tr valign="top">
				<th scope="row"><label for="tp_scroll_top_scroll_position">Position:</label></th>
				<td style="vertical-align:middle;">
				<select name="tp_scroll_top_scroll_position">
					<option value="top left" <?php if($tp_scroll_top_scroll_position=='top left') echo "selected"; ?> >Top Left</option>
					<option value="top center" <?php if($tp_scroll_top_scroll_position=='top center') echo "selected"; ?> >Top Center</option> 
					<option value="top right" <?php if($tp_scroll_top_scroll_position=='top right') echo "selected"; ?> >Top Right</option> 
					<option value="bottom left" <?php if($tp_scroll_top_scroll_position=='bottom left') echo "selected"; ?> >Bottom Left</option>
					<option value="bottom center" <?php if($tp_scroll_top_scroll_position=='bottom center') echo "selected"; ?> >Bottom Center</option> 
					<option value="bottom right" <?php if($tp_scroll_top_scroll_position=='bottom right') echo "selected"; ?> >Bottom Right</option>                   
				</select><br>
				<span style="font-size:12px;color:#000000">Use dropdown menu to select scroll button position.</span>
				</td>
			</tr>
        </table>

		<p class="submit">
			<input class="button button-primary" type="submit" name="Submit" value="<?php _e('Save Changes' ) ?>" />
		</p>

	</form>


	<script>	
		jQuery(document).ready(function($)
		{	
		jQuery('#scroll-bg, #scroll-hoverbg').wpColorPicker();
		});
	</script> 

	</div>
